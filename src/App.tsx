import Nav  from "./components/Nav" 
import { Routes, Route, HashRouter,Outlet } from "react-router-dom"
import ToggleUsage from "./Pages/ToggleUsage"
import Home from "./Pages/Home"

const Layout = () => {
  return(
    <>
      <Nav/>
      {/* 更換區段 */}
      <Outlet/> 
    </>

  )
}

function App() {


  return (
    <HashRouter>
      <Routes>
        <Route path='/' element={<Layout/>}>
        <Route path="/" element={<Home/>} />
          <Route path="/toggle" element={<ToggleUsage/>} />
        </Route>
      </Routes>
    </HashRouter>
  )
}

export default App
