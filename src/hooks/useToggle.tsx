import { Dispatch, SetStateAction, useCallback, useState } from "react";

function useToggle(defaultValue?: boolean): [
  boolean, // value
  () => void, // toggle
  Dispatch<SetStateAction<boolean>> //setValue
] {
  const [value, setValue] = useState(!!defaultValue)

  const toggle = useCallback(() => setValue(prev => !prev), [])

  return [value, toggle, setValue]
}

export default useToggle