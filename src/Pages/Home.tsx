import React from 'react'

const Home = () => {
  return (
    <div className='text-center py-5'>
      <h1>參考自 <code>usehooks-ts</code> </h1>
      <a href='https://usehooks-ts.com/' className='text-decoration-none text-warning' target='_blank'>
        https://usehooks-ts.com/
      </a>
    </div>
  )
}

export default Home