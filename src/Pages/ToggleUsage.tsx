import useToggle from '../hooks/useToggle'

const ToggleUsage = () => {
  const [value, toggle, setValue] = useToggle()

  return (
    <div className='container py-3'>
      <h1>useToggle - Usage</h1>
      <button
        className={`btn ${value ? 'btn-primary' : 'btn-danger'}`}
        onClick={toggle}
      >
        {value ? 'now true' : 'now false'}
      </button>
      <p>測試的 Toggle 值 : {value.toString()}</p>
    </div>
  )
}

export default ToggleUsage