import useToggle from "../hooks/useToggle"

const Nav = () => {

  const [value, toggle, setValue] = useToggle()
  
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">Navbar</a>
        <button className="navbar-toggler" type="button" onClick={toggle}>
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className={`collapse navbar-collapse ${ value? 'd-none' :'d-block'}`}>
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" href="/#/toggle">Toggle</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Fetch</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav